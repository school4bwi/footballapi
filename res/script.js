let url = "https://api.football-data.org/v2/competitions/";
let token = "819babcd7902454f930c154272296d78"


function getCompetition(){
    let id = document.getElementById("iptComp").value;

    if(id == "") alert("Bitte geben Sie eine ID ein!");
    else{
        
        fetch(url+id, { method: "GET", headers: { "x-auth-token": token } }).then(res => res.json()).then(data => {
            console.log(data);
            document.getElementById("compInfo").innerHTML = `<li><b>Name:</b> ${data.name}</li><li><b>Location:</b> ${data.area.name}</li><li><b>Season Count:</b> ${data.seasons.length}</li>`

            document.getElementById("tableMatches").innerHTML = "";
            document.getElementById("tableSeasons").innerHTML = `<tr><th>ID</th><th>Start</th><th>End</th><th>Winner</th><th>Image</th></tr>`

            
            for(let i = 0; i < data.seasons.length; i++){
                let start = data.seasons[i].startDate;
                let end = data.seasons[i].endDate;
                let winner = "Unbekannt";
                let image = null;
                if(data.seasons[i].winner) winner = data.seasons[i].winner.name;
                if(data.seasons[i].winner) image = data.seasons[i].winner.crestUrl;
                let id = data.seasons[i].id;

                if(image) document.getElementById("tableSeasons").innerHTML += `<tr onclick="getMatches(${id})"><td><p>${id}</p></td><td><p>${start}</p></td><td><p>${end}</p></td><td><p>${winner}</p></td><td><img src="${image}"></td></tr>`
                else document.getElementById("tableSeasons").innerHTML += `<tr onclick="getMatches(${id})"><td><p>${id}</p></td><td><p>${start}</p></td><td><p>${end}</p></td><td><p>${winner}</p></td><td><p>Nicht Verfügbar</p></td></tr>`;
            }

        });
    }
}

function getMatches(SeasonId){
    let id = document.getElementById("iptComp").value;

    fetch(url+id+"/matches", { method: "GET", headers: { "x-auth-token": token }}).then(res => res.json()).then(data => {
        console.log(data);

        document.getElementById("tableMatches").innerHTML = `<tr><th>Date</th><th>Status</th><th>Stage</th><th>Group</th><th>Home-Team</th><th>Away-Team</th></tr>`

        for(let i = 0; i < data.matches.length; i++){
            if(data.matches[i].season.id == SeasonId){

                let date = data.matches[i].utcDate;
                let status = data.matches[i].status;
                let stage = data.matches[i].stage;
                let group = "-"
                if(data.matches[i].group.includes("Group")) group = data.matches[i].group;
                let home = data.matches[i].homeTeam.name;
                let away = data.matches[i].awayTeam.name;


                if(data.matches[i].score.winner == "AWAY_TEAM"){
                    document.getElementById("tableMatches").innerHTML += `<tr><td><p>${date}</p></td><td><p>${status}</p></td><td><p>${stage}</p></td><td><p>${group}</p></td><td><p>${home}</p></td><td style="color: #6eeb34"><p>${away}</p></td></tr>`;
                }
                else if (data.matches[i].score.winner == "HOME_TEAM"){
                    document.getElementById("tableMatches").innerHTML += `<tr><td><p>${date}</p></td><td><p>${status}</p></td><td><p>${stage}</p></td><td><p>${group}</p></td><td style="color: #6eeb34"><p>${home}</p></td><td><p>${away}</p></td></tr>`;
                }
                else {
                    document.getElementById("tableMatches").innerHTML += `<tr><td><p>${date}</p></td><td><p>${status}</p></td><td><p>${stage}</p></td><td><p>${group}</p></td><td><p>${home}</p></td><td><p>${away}</p></td></tr>`;
                }
            }
        }
        
        
    });



}