Sehr geehrter Herr Riedmann,

mir ist ist im laufe des Projekts aufgefallen, dass die Daten nicht sehr zuverlässig sind.
Es gibt z.B. nur für sehr wenige Seasons einer Competition auch wirklich Daten zu den einzelnen
Matches. Auch die Gewinner der Seasons bzw. Matches sind auch sehr oft einfach NULL gewesen.
Außerdem ist mir aufgefallen, dass wenn man zu schnell hintereinander die API callt, man für eine Gewisse Zeit
geblockt wird. (Error Message steht in der Konsole)
Es tut mir also leid, wenn einige Daten nicht laden, das ist nicht der Fehler meines Programmes, sondern der der API.

Damit sie nicht lange suchen müssen:
Bei der Season 1 der Competition 2000 sind alle Daten vollständig, sie können also um das Programm zu testen diese Eingaben tätigen.

Allgemeine Verwendung des Programmes:
Wenn Sie eine Id eingeben erscheint eine Tabelle mit einer Übersicht über die verschiedenen Seasons (sofern die Id stimmt). Auf jede Zeile kann geklickt werden.
Es erscheint dann eine zweite Tabelle mit einer Übersicht über die ganzen Matches in dieser Season (sofern die Daten vorhanden sind).

LG